apiVersion: v1
kind: ConfigMap
metadata:
  name: {{.Release.Name}}
  namespace: {{.Release.Namespace}}
data:
  values.yaml: |-
    replicaCount: {{if .Values.drsite }}0{{ else }}3{{ end }}

    image:
      main:
        repo: docker.familyhartley.com/jonhartley/k8s-pxc
        tag: v5.7.34
        pullPolicy: Always
      exporter:
        repo: docker.familyhartley.com/prom/mysqld-exporter
        tag: v0.12.1
        pullPolicy: IfNotPresent
      proxysql:
        repo: proxysql/proxysql
        tag: latest
        pullPolicy: IfNotPresent
      replica:
        repo: docker.familyhartley.com/jonhartley/percona-server
        tag: latest
        pullPolicy: IfNotPresent
      maint:
        repo: docker.familyhartley.com/jonhartley/k8s-pxc-maint
        tag: v5.7.34.1
        pullPolicy: IfNotPresent
      fluent:
        repo: docker.familyhartley.com/jonhartley/k8s-pxc-fluentd
        tag: latest
        pullPolicy: IfNotPresent

    pxc:
      env:
        config: "max_allowed_packet=1G,innodb_buffer_pool_size=1G,max_connections=501,innodb_log_file_size=1G,innodb_log_buffer_size=128M"
        wsrepconfig: "pxc_strict_mode=PERMISSIVE,wsrep_retry_autocommit=20,wsrep_provider_options='gcache.size=1G;gcache.recover=yes'"
        binlogconfig: "expire_logs_days=7"
        rlimit: "15M"
        sstmem: "128M"
        autotune: "bufferpool"
        ssl: 
          enabled: false
          certmanager: true
          domain: "cluster.local"
        binlog: true
        startfrombackup: {{ if .Values.primary }}true{{ else }}false{{ end }}
    replica:
      env:
        config: "max_allowed_packet=1G,innodb_buffer_pool_size=512M,max_connections=500,innodb_log_file_size=512M,innodb_log_buffer_size=32M"
        binlogconfig: "expire_logs_days=7"
        rlimit:
        sstmem:
        autotune: "bufferpool"
        ssl: 
          enabled: false
        binlog: true
        startfrombackup: true

    backup:
      enabled: true
      pvname: ""
      pvdynamic: true
      storageClassName: "nfs"
      size: "30Gi"
      schedule: "full"
      interval: "hourly"
      source: "cluster"
      min: 5
      hour: "*/4"
      replica:
        enabled: {{ if .Values.drsite }}true{{ else }}false{{ end }}
        replicaCount: 1
        storage: "20Gi"
        master: {{ if .Values.drsite }}"airflow-pxc-cluster.{{ .Values.primary }}"{{ else }}""{{ end }}

    secrets:
      replication: password
      proxymon: password
      stats: password
      radmin: "radmin"
      admin: "admin"

    audit:
      enabled: false
      events: ""
      config: "audit_log_format=JSON"
      output: "es"
      es:
        host: 192.168.3.250
        port: 9200
        index: "mysqld"
      splunk:
        host:
        port: 8080
        token:
        index: "mysqld"

    resources: 
    {{- if .Values.production }}
      pxc:
        memory: "3Gi"
        cpu: "500m"
      proxysql:
        memory: "512Mi"
        cpu: "500m"
      replica:
        memory: "3Gi"
        cpu: "500m"
    {{- else }}
      pxc:
        memory: "1500Mi"
        cpu: "250m"
      proxysql:
        memory: "512Mi"
        cpu: "500m"
      replica:
        memory: "1500Mi"
        cpu: "250m"
    {{- end }}

    datavolume:
      storageClassName: "ssd-work"
      storage: "10Gi"

    antiaffinity:
      pxc:
        enabled: true

    nodeselector:
      enabled: false

    tolerations:
      enabled: false

    sqlinit:
      enabled: true

    initgrants: true
    defaulthost: {{ .Values.pod_subnet }}
    defaultliquibasehost: {{ .Values.pod_subnet }}
    usersecrets:
      enabled: true
    schemas:
      - name: "airflow"
        options: "CHARACTER SET utf8 COLLATE utf8_general_ci"
        liquibase: true

    grants:
      # airflow
      - user: "airflow"
        schema: "airflow"
        grants: "SELECT,UPDATE,INSERT,DELETE"
        gensecret: true
      - user: "airflow_liquibase"
        schema: "airflow"
        grants: "SELECT,UPDATE,INSERT,DELETE,CREATE,DROP,REFERENCES,INDEX,ALTER,ALTER ROUTINE,CREATE VIEW,CREATE ROUTINE,TRIGGER,CREATE TEMPORARY TABLES"
        gensecret: true
      - user: "airflow_liquibase"
        schema: "*"
        grants: "PROCESS,REPLICATION CLIENT,REPLICATION SLAVE"
        gensecret: true

